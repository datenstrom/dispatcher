# Dispatcher

Toy simulation of a dispatcher using a priority queue system. 

New processes are entered with priority included (numbering is automatic). Processes are also to be terminated by command. Context switches are by command with the cause of the switch being either a blocking call, time slice exceeded or termination. Assumes only one CPU.

Functionality:

1. Priority based Ready Queue(s).
2. Blocked list.
3. Output of complete system status after every context switch showing ready, blocked, and running processes.

## Dependencies

SDL2:

    sudo apt-get install libsdl2-dev

## Preview

![preview image](https://gitlab.com/datenstrom/dispatcher/raw/master/assets/screenshots/dispatcher.png)
