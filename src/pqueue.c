/** This is a terrible priority queue dirty hack for a class.
 *
 * It is just a sorted linked list with pop/push.
 *
 * Do not use it.
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../inc/pqueue.h"

// New pqueue on stack
struct Pqueue new_pqueue() {
    struct Pqueue pqueue;
    pqueue.head = NULL;
    pqueue.current = NULL;
    return pqueue;
}

//display the list
void display(struct Pqueue *pq) {
    struct Process *ptr = pq->head;
    printf("\n[ ");

    //start from the beginning
    while(ptr != NULL) {
        printf("(%d,%d) ", ptr->pid, ptr->priority);
        ptr = ptr->next;
    }

    printf(" ]");
}

// insert link at the first location
void push(struct Pqueue *pq, int pid, int priority, int time) {
    //create a link
    struct Process *link = (struct Process*) malloc(sizeof(struct Process));

    link->pid = pid;
    link->priority = priority;
    link->time = time;

    //point it to old first node
    link->next = pq->head;

    //point first to new first node
    pq->head = link;
    sort(pq);
}

// Remove and return first item
struct Process pop(struct Pqueue *pq) {

    //save reference to first link
    struct Process* temp_link = pq->head;
    struct Process link_copy;

    link_copy.pid = temp_link->pid;
    link_copy.time = temp_link->time;
    link_copy.priority = temp_link->priority;
    link_copy.next = temp_link->next;

    //mark next to first link as first 
    pq->head = pq->head->next;

    //return the deleted link
    free(temp_link);
    return link_copy;
}

// Returns true if list is empty
bool empty(struct Pqueue *pq) {
    return pq->head == NULL;
}

// Returns number of elements in list
int length(struct Pqueue *pq) {
    int length = 0;
    struct Process *current;

    for(current = pq->head; current != NULL; current = current->next) {
      length++;
    }

    return length;
}

//find a link with given pid
struct Process* find(struct Pqueue *pq, int pid) {

    //start from the first link
    struct Process* current = pq->head;

    //if list is empty
    if(pq->head == NULL) {
        return NULL;
    }

    //navigate through list
    while(current->pid != pid) {

        //if it is last node
        if(current->next == NULL) {
            return NULL;
        } else {
            //go to next link
            current = current->next;
        }
    }      

    //if pid found, return the current Link
    return current;
}

//delete a link with given pid 
struct Process remove_pid(struct Pqueue *pq, int pid) {

    //start from the first link
    struct Process* current = pq->head;
    struct Process* previous = NULL;

    //if list is empty
    if(pq->head == NULL) {
        struct Process empty;
        return empty;
    }

    //navigate through list
    while(current->pid != pid) {

        //if it is last node
        if(current->next == NULL) {
            struct Process empty;
            return empty;
        } else {
            //store reference to current link
            previous = current;
            //move to next link
            current = current->next;
        }
    }

    //found a match, update the link
    if(current == pq->head) {
        //change first to point to next link
        pq->head = pq->head->next;
    } else {
        //bypass the current link
        previous->next = current->next;
    }    

    struct Process link_copy;
    link_copy.pid = current->pid;
    link_copy.time = current->time;
    link_copy.priority = current->priority;
    link_copy.next = current->next;
    free(current);

    return link_copy;
}

void sort(struct Pqueue *pq) {

    int i, j, k, temp_pid, temp_priority;
    struct Process *current;
    struct Process *next;

    int size = length(pq);
    k = size ;
	
    for (i = 0 ; i < size - 1 ; i++, k--) {
        current = pq->head;
        next = pq->head->next;
		
        for (j = 1 ; j < k ; j++) {   

         if (current->priority < next->priority) {
            temp_priority = current->priority;
            current->priority = next->priority;
            next->priority = temp_priority;

            temp_pid = current->pid;
            current->pid = next->pid;
            next->pid = temp_pid;
         }
            
            current = current->next;
            next = next->next;
        }
    }
}

void reverse(struct Pqueue *pq, struct Process** head_ref) {
    struct Process* prev   = NULL;
    struct Process* current = *head_ref;
    struct Process* next;

    while (current != NULL) {
        next = current->next;
        current->next = prev;   
        prev = current;
        current = next;
    }

    *head_ref = prev;
}
