#include "../inc/overview.h"

int overview(struct nk_context *ctx, struct Pqueue *rq, struct Pqueue *bq, struct Process *running_proc)
{
    /* window flags */
    static int border = nk_true;
    static int resize = nk_true;
    static int movable = nk_true;
    static int no_scrollbar = nk_false;
    static int scale_left = nk_false;
    static nk_flags window_flags = 0;
    static int minimizable = nk_true;
    static enum nk_style_header_align header_align = NK_HEADER_RIGHT;

    /* window flags */
    window_flags = 0;
    ctx->style.window.header.align = header_align;
    if (border) window_flags |= NK_WINDOW_BORDER;
    if (resize) window_flags |= NK_WINDOW_SCALABLE;
    if (movable) window_flags |= NK_WINDOW_MOVABLE;
    if (no_scrollbar) window_flags |= NK_WINDOW_NO_SCROLLBAR;
    if (scale_left) window_flags |= NK_WINDOW_SCALE_LEFT;
    if (minimizable) window_flags |= NK_WINDOW_MINIMIZABLE;

    if (nk_begin(ctx, "Overview", nk_rect(260, 50, 480, 700), window_flags))
    {
        int i;
        nk_layout_space_begin(ctx, NK_STATIC, 500, 64);
        nk_layout_space_push(ctx, nk_rect(0,0,450,200));
        if (nk_group_begin(ctx, "group_running", NK_WINDOW_BORDER)) {
            static int selected[32];
            nk_layout_row_static(ctx, 18, 100, 1);
            nk_label(ctx, "Running:", NK_TEXT_LEFT);

            char pid_str[40];
            char pri_str[40];
            sprintf(pid_str, "PID: %d", running_proc->pid);
            sprintf(pri_str, "Priority: %d", running_proc->priority);

            nk_layout_row_dynamic(ctx, 25, 2);
            nk_label(ctx, pid_str, NK_TEXT_LEFT);
            nk_label(ctx, pri_str, NK_TEXT_RIGHT);

            nk_group_end(ctx);
        }

        nk_layout_space_push(ctx, nk_rect(0,205,225,440));
        if (nk_group_begin(ctx, "group_ready", NK_WINDOW_BORDER)) {
            nk_layout_row_static(ctx, 18, 100, 1);
            nk_label(ctx, "Ready:", NK_TEXT_LEFT);
            nk_layout_row_dynamic(ctx, 25, 2);

            struct Process *current = rq->head;
            for (i = 0; i < length(rq); ++i) {

                char pid_str[40];
                char pri_str[40];
                sprintf(pid_str, "PID: %d", current->pid);
                sprintf(pri_str, "Priority: %d", current->priority);

                nk_label(ctx, pid_str, NK_TEXT_LEFT);
                nk_label(ctx, pri_str, NK_TEXT_RIGHT);
                current = current->next;
            }
            nk_group_end(ctx);
        }

        nk_layout_space_push(ctx, nk_rect(225,205,225,440));
        if (nk_group_begin(ctx, "group_blocked", NK_WINDOW_BORDER)) {
            nk_layout_row_static(ctx, 18, 100, 1);
            nk_label(ctx, "Blocked:", NK_TEXT_LEFT);
            nk_layout_row_dynamic(ctx, 25, 2);
            struct Process *current = bq->head;
            for (i = 0; i < length(bq); ++i) {

                char pid_str[40];
                char pri_str[40];
                sprintf(pid_str, "PID: %d", current->pid);
                sprintf(pri_str, "Priority: %d", current->priority);

                nk_label(ctx, pid_str, NK_TEXT_LEFT);
                nk_label(ctx, pri_str, NK_TEXT_RIGHT);
                current = current->next;
            }
            nk_group_end(ctx);
        }

        nk_layout_space_end(ctx);
    }
    nk_end(ctx);
    return !nk_window_is_closed(ctx, "Overview");
}
