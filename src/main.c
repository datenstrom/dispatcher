#define NK_IMPLEMENTATION
#define NK_SDL_GL2_IMPLEMENTATION
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#include "../inc/main.h"
#include "../inc/overview.h"
#include "../inc/pqueue.h"

/* ===============================================================
 *
 *                       DISPATCHER
 *
 * ===============================================================
 *  A toy example of process scheduling.
 */
/*#include "../style.c"*/

int main(int argc, char* argv[])
{
    /* Platform */
    SDL_Window *win;
    SDL_GLContext glContext;
    struct nk_color background;
    int win_width, win_height;
    int running = 1;

    /* GUI */
    struct nk_context *ctx;

    /* SDL setup */
    SDL_SetHint(SDL_HINT_VIDEO_HIGHDPI_DISABLED, "0");
    SDL_Init(SDL_INIT_VIDEO);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    win = SDL_CreateWindow("Dispatcher",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_RESIZABLE|SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN|SDL_WINDOW_ALLOW_HIGHDPI);
    glContext = SDL_GL_CreateContext(win);
    SDL_GetWindowSize(win, &win_width, &win_height);

    /* GUI */
    ctx = nk_sdl_init(win);

    /* Load Fonts: if none of these are loaded a default font will be used  */
    {struct nk_font_atlas *atlas;
    nk_sdl_font_stash_begin(&atlas);
    struct nk_font *iosevka = nk_font_atlas_add_from_file(atlas, "../assets/fonts/iosevka-light.ttf", 22, 0);
    nk_style_set_font(ctx, &iosevka->handle);
    nk_sdl_font_stash_end();}

    /* Dispatcher */
    static int pid_counter;
    struct Pqueue rq = new_pqueue();  // Ready Queue
    struct Pqueue bq = new_pqueue();  // Blocked Queue

    background = nk_rgb(28,48,62);
    while(running)
    {
        /* Input */
        SDL_Event evt;
        nk_input_begin(ctx);
        while(SDL_PollEvent(&evt)) {
            if(evt.type == SDL_QUIT) goto cleanup;
            nk_sdl_handle_event(&evt);
        }
        nk_input_end(ctx);

        static struct Process running_proc;

        /* GUI */
        if(nk_begin(ctx, "Dispatch", nk_rect(0, 0, 210, win_height),
            NK_WINDOW_BORDER|NK_WINDOW_TITLE)) 
        {
            time_t t;
            srand((unsigned) time(&t));

            enum {RANDOM, MANUAL};
            enum {TIME_SLICE, BLOCKING_CALL, TERMINATION};
            static int switch_type = TIME_SLICE;
            static int op = MANUAL;
            static int priority = 42;
            static int process_time = 5;
            static int show_app_about = nk_false;

            // Menu
            nk_menubar_begin(ctx);
            nk_layout_row_begin(ctx, NK_STATIC, 25, 4);
            nk_layout_row_push(ctx, 45);
            if(nk_menu_begin_label(ctx, "MENU", NK_TEXT_LEFT, nk_vec2(120, 200)))
            {
                nk_layout_row_dynamic(ctx, 25, 1);
                if(nk_menu_item_label(ctx, "About", NK_TEXT_LEFT))
                    show_app_about = nk_true;
                nk_menu_end(ctx);
            }
            nk_layout_row_push(ctx, 70);
            nk_menubar_end(ctx);

            if(show_app_about)
            {
                /* about popup */
                static struct nk_rect app_about_size = {20, 100, 300, 190};
                if(nk_popup_begin(ctx, NK_POPUP_STATIC, "About", NK_WINDOW_CLOSABLE, app_about_size))
                {
                    nk_layout_row_dynamic(ctx, 20, 1);
                    nk_label(ctx, "Dispatcher", NK_TEXT_LEFT);
                    nk_label(ctx, "By Derek Goddeau", NK_TEXT_LEFT);
                    nk_layout_row_static(ctx, 100, 200, 1);
                    nk_label_wrap(ctx, "Dispatcher is licensed under the GNU GPLv3.");
                    nk_popup_end(ctx);
                } else show_app_about = nk_false;
            }

            if (nk_tree_push(ctx, NK_TREE_TAB, "Add Process", NK_MINIMIZED))
            {
                // radio buttons
                nk_layout_row_dynamic(ctx, 30, 2);
                if(nk_option_label(ctx, "manual", op == MANUAL)) op = MANUAL;
                if(nk_option_label(ctx, "random", op == RANDOM)) {
                    op = RANDOM;
                    priority = rand() % 100;
                    process_time = rand() % 10 + 1;
                }

                // properties
                nk_layout_row_dynamic(ctx, 25, 1);
                nk_property_int(ctx, "Priority:", 0, &priority, 99, 1, 1);
                nk_property_int(ctx, "Time:", 1, &process_time, 10, 1, 1);

                // buttons
                nk_layout_row_dynamic(ctx, 30, 2);
                if(nk_button_label(ctx, "add")) {
                    push(&rq, pid_counter, priority, process_time);
                    if(op == RANDOM) {
                        priority = rand() % 100;
                        process_time = rand() % 10 + 1;
                    }
                    pid_counter++;
                }
                nk_button_set_behavior(ctx, NK_BUTTON_REPEATER);
                if(nk_button_label(ctx, "fill")) {
                    push(&rq, pid_counter, priority, process_time);
                    if(op == RANDOM) {
                        priority = rand() % 100;
                        process_time = rand() % 10 + 1;
                    }
                    pid_counter++;
                }
                nk_button_set_behavior(ctx, NK_BUTTON_DEFAULT);
                nk_tree_pop(ctx);
            }

            if(nk_tree_push(ctx, NK_TREE_TAB, "Context Switch", NK_MINIMIZED))
            {
                nk_label(ctx, "Type:", NK_TEXT_LEFT);
                nk_layout_row_dynamic(ctx, 30, 1);
                if(nk_option_label(ctx, "time slice", switch_type == TIME_SLICE))
                    switch_type = TIME_SLICE;
                if(nk_option_label(ctx, "termination", switch_type == TERMINATION))
                    switch_type = TERMINATION;
                if(nk_option_label(ctx, "blocking call", switch_type == BLOCKING_CALL))
                    switch_type = BLOCKING_CALL;

                // buttons
                nk_layout_row_dynamic(ctx, 30, 2);
                if(nk_button_label(ctx, "Switch")) {
                    if(switch_type == TERMINATION) {
                        if(rq.head != NULL) {
                            running_proc = pop(&rq);
                        }
                    } else if(switch_type == BLOCKING_CALL) {
                        push(&bq, running_proc.pid, running_proc.priority, running_proc.time);
                        if(rq.head != NULL) {
                            running_proc = pop(&rq);
                        }
                    } else if(switch_type == TIME_SLICE) {
                        push(&rq, running_proc.pid, running_proc.priority, running_proc.time);
                        if(rq.head != NULL) {
                            running_proc = pop(&rq);
                        }
                    }
                }
                nk_button_set_behavior(ctx, NK_BUTTON_REPEATER);
                if(nk_button_label(ctx, "empty")) {
                    if(rq.head != NULL) {
                        if(switch_type == TERMINATION)
                            running_proc = pop(&rq);
                    }
                }
                nk_button_set_behavior(ctx, NK_BUTTON_DEFAULT);
                nk_tree_pop(ctx);
            }
        }
        nk_end(ctx);

        /* -------------- Panel ---------------- */
        overview(ctx, &rq, &bq, &running_proc);
        /* ----------------------------------------- */

        /* Draw */
        {float bg[4];
        nk_color_fv(bg, background);
        SDL_GetWindowSize(win, &win_width, &win_height);
        glViewport(0, 0, win_width, win_height);
        glClear(GL_COLOR_BUFFER_BIT);
        glClearColor(bg[0], bg[1], bg[2], bg[3]);
        /* IMPORTANT: `nk_sdl_render` modifies some global OpenGL state
         * with blending, scissor, face culling, depth test and viewport and
         * defaults everything back into a default state.
         * Make sure to either a.) save and restore or b.) reset your own state after
         * rendering the UI. */
        nk_sdl_render(NK_ANTI_ALIASING_ON);
        SDL_GL_SwapWindow(win);}
    }

cleanup:
    nk_sdl_shutdown();
    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(win);
    SDL_Quit();
    return 0;
}
