TARGET = dispatcher

CC = gcc
CFLAGS = -g -std=c11 -Wall -I. -pedantic -O2
LINKER = gcc -o
LFLAGS = -Wall -I. -lm
LIBS = -lSDL2 -lGL -lm -lGLU

SRCDIR = src
INCDIR = inc
OBJDIR = target/obj
BINDIR = target/bin
TESTDIR = tests

SOURCES := $(wildcard $(SRCDIR)/*.c)
SOURCES_TESTS := $(wildcard $(TESTDIR)/*.c)
INCLUDES_TEST := $(wildcard $(TESTDIR)/*.h)
INCLUDES := $(wildcard $(INCDIR)/*.h)
OBJECTS := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
RM = rm -rf



all: directories build link 


.PHONY: directories
directories: ${OUT_DIR}
	mkdir -p ${OBJDIR} ${BINDIR}

${OUT_DIR}:
        ${MKDIR_P} ${OUT_DIR}

.PHONY: link
link: $(BINDIR)/$(TARGET)

$(BINDIR)/$(TARGET): $(OBJECTS)
	@$(LINKER) $@ $(LFLAGS) $(OBJECTS) $(LIBS)
	@echo "Linking complete!"

.PHONY: build
build: $(OBJECTS)

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled "$<" successfully!"

.PHONY: clean
clean:
	@$(RM) $(OBJECTS)
	@echo "Cleanup complete!"

.PHONY: remove
remove: clean
	@$(RM) $(BINDIR)/$(TARGET)
	@echo "Executable removed!"
