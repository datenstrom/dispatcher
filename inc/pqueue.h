/** This is a terrible priority queue dirty hack for a class.
 *
 * Do not use it.
 *
 */
#pragma once

#include <stdbool.h>

struct Process {
    int pid;
    int time;
    int priority;
    struct Process *next;
};

struct Pqueue {
    struct Process *head;
    struct Process *current;
};

struct Pqueue new_pqueue();
void display(struct Pqueue *pq);
void push(struct Pqueue *pq, int pid, int priority, int time);
struct Process pop(struct Pqueue *pq);
bool empty(struct Pqueue *pq);
int length(struct Pqueue *pq);
struct Process* find(struct Pqueue *pq, int pid);
struct Process remove_pid(struct Pqueue *pq, int pid);
void sort(struct Pqueue *pq);
void reverse(struct Pqueue *pq, struct Process** head_ref);
